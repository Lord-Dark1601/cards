package cards;

public class Deck {

	private Card[] deck;
	private int cardExtract;

	public Deck() {
		deck = new Card[48];
		cardExtract = 48;
		for (int i = 0; i < 12; i++) {
			deck[i] = new Card(i + 1, Suit.BASTOS);
			deck[i + 12] = new Card(i + 1, Suit.COPAS);
			deck[i + 24] = new Card(i + 1, Suit.ESPADAS);
			deck[i + 36] = new Card(i + 1, Suit.OROS);
		}
	}

	public void shuffle() {
		for (int i = 0; i < deck.length * 10; i++) {
			int x = (int) (Math.random() * cardExtract);
			int y = (int) (Math.random() * cardExtract);
			Card c = deck[y];
			deck[y] = deck[x];
			deck[x] = c;
		}
	}

	public void print() {
		for (int i = 0; i < deck.length; i++) {
			System.out.println(deck[i]);
		}
	}

	public Card extractCard() {
		cardExtract--;
		Card c = deck[cardExtract];
		return c;
	}
}
