package cards;

public class Card {

	private int number;
	private Suit suit;

	public Card(int number, Suit suit) {
		this.number = number;
		this.suit = suit;
	}

	@Override
	public String toString() {
		String s;
		switch (number) {
		case 1:
			s = "As";
			break;
		case 10:
			s = "Sota";
			break;
		case 11:
			s = "Caballo";
			break;
		case 12:
			s = "Rey";
			break;
		default:
			s = "" + number;
			break;
		}
		switch (suit) {
		case OROS:
			s += " de oros";
			break;
		case COPAS:
			s += " de copas";
			break;
		case ESPADAS:
			s += " de espadas";
			break;
		case BASTOS:
			s += " de bastos";
			break;
		}
		return s;
	}
}
